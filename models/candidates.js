var mongoose = require('mongoose'); //Import Mongoose Package
var Schema = mongoose.Schema; //Assign Monggose Schema function to variable
var bcrypt = require('bcryptjs'); // Import Bcrypt Package

//Candidate Mongoose Schema
// var CandidateSchema = new Schema({
//     name: { type: String, required:true },
//     username: { type: String, lowercase: true, required:true, unique:true },
//     password: { type: String, required:true },
//     email: { type: String, required:true, lowercase: true, unique: true },
//     active: { type: Boolean, required: true, default: false },
//     temporarytoken: { type: String, required:true }, 
//     resettoken: { type: String, required:false },
//     permission: { type: String, required:true, default: 'candidate'},
//     profilePic: { type: String, required:false, default: null }
// });


//User Schema
const CandidateSchema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
});


module.exports = mongoose.model('Candidate', CandidateSchema); //Export candidate Model for us  in API