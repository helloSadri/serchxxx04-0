const express = require('express');
const router = express.Router();
// const jwt = require('jsonwebtoken')

const Candidate = require('../../models/candidates')

//Search
// router.get('/search', (req, res, next) => {
//     res.send('SEARCH');
// });

// router.get('/search', function (req, res) {
//     Candidate.findOne({
//         username: 'sadri'
//     }, function (err, candidate) {
//         if (err) {
//             res.send(err);
//         } else {
//             if (!candidate) {
//                 res.send('Your not there !');
//             } else {
//                 res.send(candidate);
//             }
//         }
//     })
// });

// router.get('/search/:username', function (req, res) {
//     Candidate.findOne({
//         username: req.params.username
//     }, function (err, candidate) {
//         if (err) {
//             res.send(err);
//         } else {
//             if (!candidate) {
//                 res.send('Your not there !');
//             } else {
//                 res.send(candidate);
//             }
//         }
//     })
// });

// Request for all Candidates
router.get('/candidates/', function (req, res) {
    console.log('Get request for all Candidates')
    Candidate.find({})
        .exec(function (err, candidates) {
            if (err) {
                console.log("Error retrieving candidates");
            } else {
                res.json(candidates)
            }
        });
});

// Request for a single Candidate
router.get('/candidate/:id', function (req, res) {
    console.log('Get request for a single Candidate')
    Candidate.findById(req.params.id)
        .exec(function (err, candidate) {
            if (err) {
                console.log("Error retrieving the candidate");
            } else {
                res.json(candidate)
            }
        });
});



// Profile
// router.get('/profile', (req, res, next) => {
//     res.send('PROFILE');
// });


module.exports = router;