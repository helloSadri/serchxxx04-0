const express = require('express');
const router = express.Router();
// const jwt = require('jsonwebtoken')

const Candidate = require('../../models/candidates')


// Request for all Candidates
router.get('/candidates/', function (req, res) {
    console.log('Get request for all Candidates')
    Candidate.find({})
        .exec(function (err, candidates) {
            if (err) {
                console.log("Error retrieving candidates");
            } else {
                res.json(candidates)
            }
        });
});

// Request for a single Candidate
router.get('/candidate/:id', function (req, res) {
    console.log('Get request for a single Candidate')
    Candidate.findById(req.params.id)
        .exec(function (err, candidate) {
            if (err) {
                console.log("Error retrieving the candidate");
            } else {
                res.json(candidate)
            }
        });
});


module.exports = router;