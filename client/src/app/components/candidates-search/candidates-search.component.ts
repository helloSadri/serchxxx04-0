import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject'

import { Candidate } from "../../class/candidate";
import { CandidateSearchService } from 'app/services/candidate-search.service';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


@Component({
  selector: 'app-candidates-search',
  templateUrl: './candidates-search.component.html',
  styleUrls: ['./candidates-search.component.css'],
  providers: [CandidateSearchService]
})

export class CandidatesSearchComponent implements OnInit {
  candidates: Observable<Candidate[]>;
  private searchTerms = new Subject<string>();


  constructor(
    private _CandidateSearchService: CandidateSearchService,
    private router: Router) { }

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }


  ngOnInit(): void {
    this.candidates = this.searchTerms
      .debounceTime(300)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time the term changes
        // return the http search observable
        ? this._CandidateSearchService.search(term)
        // or the observable of empty heroes if there was no search term
        : Observable.of<Candidate[]>([]))
      .catch(error => {
        // TODO: add real error handling
        console.log(error);
        return Observable.of<Candidate[]>([]);
      });
  }

  gotoDetail(candidate: Candidate): void {
    let link = ['/detail', candidate.id];
    this.router.navigate(link);
  }
}


//   ngOnInit() {
//     this._CandidateSearchService.getCandidates()
//       .subscribe(resCandidateData => this.candidates = resCandidateData);
//   }
//   getSelectCandidate(candidate: any) {
//     this.selectedCandidate = candidate;
//     console.log(this.selectedCandidate);
//   }

//   searchCandidate(candidate: any) {
//     this._CandidateSearchService.searchCandidate(this.searchStr).subscribe(res => {
//       this.selectedCandidate = candidate;
//     })

//   }
// }



