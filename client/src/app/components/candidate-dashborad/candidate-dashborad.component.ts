import { Component, OnInit } from '@angular/core';

import { Candidate } from "../../class/candidate";
// import { CandidateSearchService } from '../../services/candidate-search.service';
import { CandidateService } from '../../services/candidate.service';


@Component({
  selector: 'app-candidate-dashborad',
  templateUrl: './candidate-dashborad.component.html',
  styleUrls: ['./candidate-dashborad.component.css']
})
export class CandidateDashboradComponent implements OnInit {

  candidates: Candidate[] = [];

  constructor(private candidateService: CandidateService) { }

  ngOnInit(): void {
    this.candidateService.getCandidates()
      .then(candidates => this.candidates = candidates.slice(1, 2));
  }

}
