import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Candidate } from "../../class/candidate";
// import { CandidateSearchService } from '../../services/candidate-search.service';
import { CandidateService } from '../../services/candidate.service';

@Component({
  selector: 'app-candidatedetail',
  templateUrl: './candidatedetail.component.html',
  styleUrls: ['./candidatedetail.component.css']
})
export class CandidatedetailComponent implements OnInit {
  candidate: Candidate;

  constructor(
    private candidateService: CandidateService,
    private route: ActivatedRoute,
    private location: Location
  ) { }


  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.candidateService.getCandidate(+params['id']))
      .subscribe(candidate => this.candidate = candidate);
  }

  // save(): void {
  //   this.candidateService.update(this.candidate)
  //     .then(() => this.goBack());
  // }

  goBack(): void {
    this.location.back();
  }


}
