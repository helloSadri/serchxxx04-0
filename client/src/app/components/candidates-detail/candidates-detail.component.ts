import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-candidates-detail',
  templateUrl: './candidates-detail.component.html',
  styleUrls: ['./candidates-detail.component.css'],
  inputs: ['candidate']
})
export class CandidatesDetailComponent implements OnInit {

  private editName: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.editName = false;
  }

  onNameClick() {
    this.editName = true;
  }

}
