import { Injectable } from '@angular/core';
// import { Http, Response } from '@angular/http';

import { Headers, Http } from '@angular/http';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Candidate } from "./../class/candidate";


@Injectable()
export class CandidateService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private candidatesUrl = '/api/candidates';

  constructor(private _http: Http) { }

  getCandidates() {
    return this._http.get(this.candidatesUrl)
      .toPromise()
      .then(response => response.json().data as Candidate[])
      .catch(this.handleError);
  }

  getCandidate(id: number): Promise<Candidate> {
    const url = `${this.candidatesUrl}/${id}`;
    return this._http.get(url)
      .toPromise()
      .then(response => response.json().data as Candidate)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
