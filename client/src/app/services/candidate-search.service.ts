import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/throw';
// import 'rxjs/add/operator/catch';

import 'rxjs/add/operator/map';
import { Candidate } from "./../class/candidate";

@Injectable()
export class CandidateSearchService {

  // private headers = new Headers({ 'Content-Type': 'application/json' });
  // private _getUrl = '/api/candidates';

  constructor(private http: Http) { }

  search(term: string): Observable<Candidate[]> {
    return this.http
      .get(`/api/candidates/?name=${term}`)
      .map(response => response.json().data as Candidate[]);
  }


  // getCandidate(id: number): Promise<Candidate> {
  //   const url = `${this._getUrl}/${id}`;
  //   return this.http.get(url)
  //     .toPromise()
  //     .then(response => response.json().data as Candidate)
  //     .catch(this.handleError);
  // }

  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error); 
  //   return Promise.reject(error.message || error);
  // }
}