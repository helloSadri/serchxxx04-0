export class Candidate {

    id: number;
    name: String;
    email: String;
    username: String;
    description: String;

}
